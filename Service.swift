//
//  Service.swift
//  goProInstagram
//
//  Created by Amy Acuff on 7/16/16.
//  Copyright © 2016 Winnning Edge Apps. All rights reserved.
//

import UIKit

class Service: NSObject {
    
    enum JSONError: String, ErrorType {
        case NoData = "ERROR: no data"
        case ConversionFailed = "ERROR: conversion from JSON failed"
    }
    
    
    func fetchItems(completion: (dataArray:[AnyObject], error: NSError?) -> Void ){
        
        let urlPath = "https://www.instagram.com/gopro/media/"
        guard let endpoint = NSURL(string: urlPath)
            else {
                print("Error creating endpoint")
                return
        }
        let request = NSMutableURLRequest(URL:endpoint)
        NSURLSession.sharedSession().dataTaskWithRequest(request) { (data, response, error) in
            do {
                guard let data = data
                    else {
                        throw JSONError.NoData
                }
                guard let json = try NSJSONSerialization.JSONObjectWithData(data, options: []) as? Dictionary<String, AnyObject>,
                    
                    let itemsArray = json["items"] as? Array<AnyObject>
        
                    
                    else {
                        throw JSONError.ConversionFailed
                }
                print("JSON results in fetchItems \(json)")
                completion(dataArray: itemsArray, error: error)
            } catch let error as JSONError {
                print(error.rawValue)
                
            } catch let error as NSError {
                print(error.debugDescription)
            }
            }.resume()
        
        
    }
    
    
   

}
